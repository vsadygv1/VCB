# VCB

VCB is an open-source custom PCB designed for a personal project, but I've opened it to share with everyone else. It was created by V Sadygov  as a personal project to learn PCB design and make a keyboard thats both pretty.

![VCB PCB](https://vsadygv.com/vcb.png)

# Features

- **Open-Source**: Released under an open-source license, allowing for modification and improvement by the community.
- **Customizable**: Allows for customization and expansion to suit specific project requirements.
- **Compact Design**: Designed with the ability to reflash firmware with a rotery-encoder that can be used after flashing.

## Getting Started

To get started with VCB, follow these steps:

1. **Clone the Repository**: Clone the VCB repository to your local machine with `git clone https://gitlab.com/vsadygv/VCB.git`

2. **Open in KiCAD**: Install <a href="https://www.kicad.org/" target="_blank">KiCAD</a> if you haven't, head over to `File > Open Project...` and navigate to the unziped VCB folder, and select the .kicad_pro file 

3. **Modify**: If you wish to modify anything this is where you can!

4. **Export For Printing**: Head to `File > Export > Gerbr Files` and save that output to a zip file, upload to any PCB manufacturing website you choose.


## Contributing

Contributions to VCB are welcome! If you find any issues or have suggestions for improvements, please open an issue or submit a pull request on GitLab.

## Background

I never received a formal education in electrical engineering, but it was always an interest of mine. So, I made sure to learn it on my own whenever I could. I often felt intimidated by the idea of designing a PCB. While schematics were easy enough for me, the concept of creating a PCB seemed much more daunting. I told myself that learning PCB design would be essential for embedded systems/product development, which was the driving force behind this project. Since keyboards aren't overly complex, I thought it would be a good first project to challenge myself.

## Interactive Website

To view the tracking of the PCB traces interactively, visit <a href="https://vcb.vsadygv.com" target="_blank">VCB Interactive Website</a>


## License

VCB is open-source software licensed under the [MIT License](LICENSE).

## Contact

For any inquiries or feedback, please contact V Sadygov at <a href="mailto:v@vsadygv.com">v@vsadygv.com</a>

